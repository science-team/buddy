Source: buddy
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/buddy
Vcs-Git: https://salsa.debian.org/science-team/buddy.git
Homepage: https://buddy.sourceforge.net/manual/main.html
Rules-Requires-Root: no

Package: libbdd0c2
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Binary decision-diagram library (runtime)
 Binary decision diagrams (BDDs) are space-efficient encodings of
 boolean expressions or dynamic truth tables, used in eg. model
 checking.  This is the runtime package for programs that use the
 BuDDy library.

Package: libbdd-dev
Section: libdevel
Architecture: any
Depends: libbdd0c2 (= ${binary:Version}), ${misc:Depends}, libc6-dev | libc-dev
Description: Binary decision-diagram library (development)
 Binary decision diagrams (BDDs) are space-efficient encodings of
 boolean expressions or dynamic truth tables, used in eg. model
 checking.  BuDDy is an efficient BDD library with all the standard
 BDD operations, dynamic reordering of variables, automated garbage
 collection, a C++ interface with automatic reference counting, and
 more.
 .
 libbdd-dev is the BuDDy development package containing a static
 library and the include files needed for building applications using
 BuDDy.
